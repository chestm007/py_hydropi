# py_hydropi

This is a fairly simple python daemon for controlling hydroponics equipment via a RaspberryPI.
All configuration is done via easy to understand YAML files, the example in `/py_hydropi/defaults/module_config.yaml`
outlines all possible configuration options

## Testing
environment variables:
`PY_HYDROPI_TESTING=true`
tests must be ran from the root repository directory