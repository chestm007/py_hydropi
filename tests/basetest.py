from py_hydropi.main import main as py_hydropi_main


def launch_app():
    py_hydropi_main()


def main():
    launch_app()


if __name__ == '__main__':
    main()
